import React from "react";

const headerComponent = (props) => {
  const { title, page } = props;
  return (
    <div>
      <h1 style={{ color: title }}>{title}</h1>
    </div>
  );
};

export default headerComponent;
