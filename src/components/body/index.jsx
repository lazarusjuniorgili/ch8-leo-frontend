import React from "react";
import "./style.css";

const headerComponent = ({ data, action }) => {
  //   return (
  //     <div className="card">
  //       <p className="card-header">{data.summary}</p>
  //       <p className="card-content">{data.content}</p>
  //       <button onClick={() => action(data.summary)}>Info</button>
  //     </div>
  //   );

  return (
    <div className="container">
      <div className="card" onClick={() => action(data)} style={{ backgroundColor: data }}></div>
    </div>
  );
};

export default headerComponent;
