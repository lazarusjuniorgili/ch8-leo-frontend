import React from "react";
import "./style.css";

const ListName = ({ name, handleDelete, handleUpdate }) => {
  return (
    <div className="listname-container">
      <p>{name}</p>
      <button className="tombol-update" onClick={() => handleUpdate(name)}>
        Update
      </button>
      <button className="tombol-delete" onClick={() => handleDelete(name)}>
        Delete
      </button>
    </div>
  );
};

export default ListName;
