import logo from "./logo.svg";
import "./App.css";
import Header from "./components/header";
import Body from "./components/body";
import Names from "./components/listname";
import Footer from "./components/footer";
import { useState } from "react";

function App() {
  const header = { title: "Data Input" };

  const [words, setWords] = useState("Headers");
  const [value, setValue] = useState("");
  const [name, setName] = useState("");
  const [names, setNames] = useState([]);
  const [error, setError] = useState("");
  const [isUpdate, setIsUpdate] = useState(false);
  const [search, setSearch] = useState([]);

  const handleSubmit = () => {
    const isFind = names.find((row) => row === value);
    if (!isUpdate) {
      if (!isFind) {
        setNames([...names, value]);
        setValue("");
      } else {
        setError("Name Already Taken");
        setTimeout(() => {
          setError("");
        }, 1500);
      }
    } else {
      const newNames = names.map((row) => {
        return row === name ? value : row;
      });
      setName("");
      setNames(newNames);
      setIsUpdate(false);
    }
  };

  const handleChange = (e) => {
    setValue(e.target.value);
  };

  const handleDelete = (name) => {
    const newNames = names.filter((row) => row !== name);
    setNames(newNames);
  };

  const handleUpdate = (name) => {
    setIsUpdate(true);
    setName(name);
    setValue(name);
  };

  const handleSearch = () => {
    const cari = names.filter((row) => row === value);
    setNames(cari);
  };

  return (
    <div className="app-container">
      <div>
        <Header className="header" title={words} />
        <label className="label1">Nama</label>
        <input value={value} onChange={(e) => setValue(e.target.value)} />

        {value && <button onClick={handleSubmit}>Submit</button>}
        <button onClick={handleSearch}>Search</button>
        {error && <div className="error">{error}</div>}
        <div className="container">
          {names.map((row, index) => (
            <Names name={row} key={index} handleDelete={handleDelete} handleUpdate={handleUpdate} handleSearch={handleSearch} />
          ))}
          {names.length < 1 && <div className="data">Data Empty</div>}
          {search &&
            search.map((row) => {
              <div>{search}</div>;
            })}
        </div>
      </div>
    </div>
  );
}

export default App;
